package com.example.testfbrv;

public class Food {
    private String nId;
    private String nName;
    private String nLink;

    public Food(){
    }

    public Food(String nId, String nName, String nLink) {
        this.nId = nId;
        this.nName = nName;
        this.nLink = nLink;
    }

    public String getnId() {
        return nId;
    }

    public void setnId(String nId) {
        this.nId = nId;
    }

    public String getnName() {
        return nName;
    }

    public void setnName(String nName) {
        this.nName = nName;
    }

    public String getnLink() {
        return nLink;
    }

    public void setnLink(String nLink) {
        this.nLink = nLink;
    }
}
