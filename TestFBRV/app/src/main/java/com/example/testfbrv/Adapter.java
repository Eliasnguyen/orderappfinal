package com.example.testfbrv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    Context nContext;
    ArrayList<Food> nListFood;

    public Adapter(Context nContext, ArrayList<Food> nListFood) {
        this.nContext = nContext;
        this.nListFood = nListFood;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(nContext).inflate(R.layout.item,parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Food food = nListFood.get(position);
        holder.txtId.setText(food.getnId());
        holder.txtName.setText(food.getnName());
//        holder.txtLink.setText(food.getnLink());
        Glide.with(holder.imgAva.getContext()).load(food.getnLink()).into(holder.imgAva);

    }

    @Override
    public int getItemCount() {
        return nListFood.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView txtId, txtName;
        ImageView imgAva;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtId = itemView.findViewById(R.id.tvId);
            txtName =  itemView.findViewById(R.id.tvName);
            imgAva = (ImageView)itemView.findViewById(R.id.imgAva);

        }
    }
}
